//********************************************************************

//File:	        Problem2.java       

//Author:       Liam Quinn

//Date:	        September 20th 2017

//Course:       CPS100

//

//Problem Statement:

//  Receives user's input of two floating point number and calculates 

//  the sum, difference, and product.

//Inputs:	Numerical Value (double)  

//Outputs:	Sum, Difference, Product

//

//********************************************************************

import java.util.Scanner;

public class Problem1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboardInput = new Scanner(System.in);
		
		double firstDouble = 0;
		double secondDouble = 0;
		
		System.out.println("Give first floating point number");
		firstDouble = (double)keyboardInput.nextDouble();
		System.out.println("Your first number is: " + firstDouble);
		
		System.out.println("Give second floating point number");
		secondDouble = (double)keyboardInput.nextDouble();
		System.out.println("Your second number is: " + secondDouble);
		
		System.out.println("");

		System.out.println("Your two numbers added are: " + (firstDouble + secondDouble));
		System.out.println("Your two number subtracted are: " + (firstDouble - secondDouble));
		System.out.println("Your two numbers multiplied are: " + (firstDouble * secondDouble));

		keyboardInput.close();
	}

}
