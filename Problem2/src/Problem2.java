//********************************************************************

//File:	        Problem2.java       

//Author:       Liam Quinn

//Date:	        September 14th 2017

//Course:       CPS100

//

//Problem Statement:

//  Receives user's input of money and determines fewest number of each   

//  bill and coin needed to represent that ammount

//Inputs:	Number of Seconds (double)  

//Outputs:	Time period: Number of Years, Months, Days,  Hours, Minutes

//

//********************************************************************
import java.util.Scanner;

public class Problem2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboardInput = new Scanner(System.in);
		
		double value = 0;
		int twenties = 0;
		int tens = 0;
		int fives = 0;
		int toonies = 0;
		int loonies = 0;
		int quarters = 0;
		double finalValue = 0;
		int remaining = 0;
		
		System.out.println("Type ammount of $: ");
		value = (double)keyboardInput.nextDouble();
		
		System.out.println("Your money is: $" + value);
		
		twenties = (int) (value / 20.00);
		finalValue = (double) (value - (twenties * 20.00));
		tens = (int) (finalValue / 10.00);
		finalValue = (double) (finalValue - (tens * 10.00));
		fives = (int) (finalValue / 5.00);
		finalValue = (double) (finalValue - (fives * 5.00));
		toonies = (int) (finalValue / 2.00);
		finalValue = (double) (finalValue - (toonies * 2.00));
		loonies = (int) (finalValue / 1.00);
		finalValue = (double) (finalValue - (loonies * 1.00));
		quarters = (int) (finalValue / 0.25);
		finalValue = (double) (finalValue - (quarters * 0.25));
		value = (double) value;
		finalValue = (double) finalValue;
		remaining = (int) (finalValue * 100);
		finalValue = (int) finalValue;
		
		System.out.println("Ammount of $20 bills = " + twenties);
		System.out.println("Ammount of $10 bills = " + tens);
		System.out.println("Ammount of $5 bills = " + fives);
		System.out.println("Ammount of $2 coins = " + toonies);
		System.out.println("Ammount of $1 coins = " + loonies);
		System.out.println("Ammount of �25 coins = " + quarters);
		System.out.println("Ammount of �1 remaining = " + remaining);
		
		keyboardInput.close();
	}

}
